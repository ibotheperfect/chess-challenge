# Chass Challenge #

This code written to calculate a chess problem given by try catch. 
Algorithm is finding all possible sequences and test them. We can call it brute force. 

### How to configure input ###

* To configure input simply edit config.py file

### How to run ###

* You can run code with below command

	python main.py

### Unit Testing ###

* tesy.py file is contain unit tests. You can run as;

	python test.py

### Others ###

* pylint score
	9.61/10.0
