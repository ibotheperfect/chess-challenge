"""
 Main function
 Author: Ibrahim Ercan
"""

from mychesstools import ChessChallenge
from config import *
import timeit


def main():
    """
    Main function
    """
    challenge = ChessChallenge(M, N, KING, QUEEN, BISHOP, ROOK, KNIGHT)

    start = timeit.default_timer()
    result = challenge.run()
    end = timeit.default_timer()

    estimated = end - start

    print "Result:", result
    print "Calculated in ", round(estimated, 4), "seconds"


if __name__ == "__main__":
    main()
