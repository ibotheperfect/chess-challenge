"""
    Unittest of ChessChallenge class
"""


import unittest
from mychesstools import ChessChallenge


class TestChessChallenge(unittest.TestCase):
    def test_bishop(self):
        test_challenge = ChessChallenge(3, 3, 0, 0, 0, 0, 0)

        board = [['0', 'K', '0'],
                 ['K', 'B', 'K'],
                 ['0', 'K', '0']]
        self.assertFalse(test_challenge.is_bishop_threaten(1, 1, board))
        board = [['K', '0', '0'],
                 ['0', 'B', '0'],
                 ['0', '0', '0']]
        self.assertTrue(test_challenge.is_bishop_threaten(1, 1, board))

    def test_knight(self):
        test_challenge = ChessChallenge(3, 3, 0, 0, 0, 0, 0)

        board = [['N', 'K', 'B'],
                 ['K', 'B', '0'],
                 ['K', '0', 'K']]
        self.assertFalse(test_challenge.is_knight_threaten(0, 0, board))
        board = [['N', '0', '0'],
                 ['0', 'B', '0'],
                 ['0', 'N', '0']]
        self.assertTrue(test_challenge.is_knight_threaten(0, 0, board))

    def test_queen(self):
        test_challenge = ChessChallenge(3, 3, 0, 0, 0, 0, 0)

        board = [['Q', '0', '0'],
                 ['0', '0', 'K'],
                 ['0', 'K', '0']]
        self.assertFalse(test_challenge.is_queen_threaten(0, 0, board))
        board = [['Q', '0', '0'],
                 ['0', 'B', '0'],
                 ['0', 'N', '0']]
        self.assertTrue(test_challenge.is_queen_threaten(0, 0, board))

    def test_rook(self):
        test_challenge = ChessChallenge(3, 3, 0, 0, 0, 0, 0)

        board = [['Q', '0', 'Q'],
                 ['0', 'R', '0'],
                 ['Q', '0', 'Q']]
        self.assertFalse(test_challenge.is_rook_threaten(1, 1, board))
        board = [['Q', '0', '0'],
                 ['0', 'R', '0'],
                 ['0', 'N', '0']]
        self.assertTrue(test_challenge.is_rook_threaten(1, 1, board))

    def test_king(self):
        test_challenge = ChessChallenge(3, 3, 0, 0, 0, 0, 0)

        board = [['K', '0', 'Q'],
                 ['0', '0', '0'],
                 ['Q', '0', 'Q']]
        self.assertFalse(test_challenge.is_king_threaten(0, 0, board))
        board = [['Q', '0', '0'],
                 ['0', 'K', '0'],
                 ['0', 'N', '0']]
        self.assertTrue(test_challenge.is_king_threaten(1, 1, board))

    def test_coord_threaten(self):
        test_challenge = ChessChallenge(3, 3, 0, 0, 0, 0, 0)
        board = [['K', '0', 'Q'],
                 ['0', '0', '0'],
                 ['Q', '0', 'Q']]
        self.assertTrue(test_challenge.is_coord_threaten(0, 0, board))
        self.assertFalse(test_challenge.is_coord_threaten(1, 1, board))
        self.assertFalse(test_challenge.is_coord_threaten(1, -1, board))

    def test_convert2d(self):
        test_challenge = ChessChallenge(3, 3, 0, 0, 0, 0, 0)
        list_1d = ['K', '0', 'B', '0', 'N', 'Q', 'R', '0', '0']
        list_2d = [['K', '0', 'B'], ['0', 'N', 'Q'], ['R', '0', '0']]

        self.assertEqual(test_challenge.convert_2d(list_1d), list_2d)

    def test_permutation(self):
        test_challenge = ChessChallenge(3, 3, 0, 0, 0, 0, 0)

        count = 0
        for i in test_challenge.unique_permutations():
            count += 1
        self.assertEqual(count, 1)

        test_challenge = ChessChallenge(3, 3, 1, 0, 0, 0, 0)

        count = 0
        for i in test_challenge.unique_permutations():
            count += 1
        self.assertEqual(count, 9)

        test_challenge = ChessChallenge(3, 3, 1, 1, 0, 0, 0)

        count = 0
        for i in test_challenge.unique_permutations():
            count += 1
        self.assertEqual(count, 72)

    def test_run(self):

        #  tested with examples in problem pdf
        test_challenge = ChessChallenge(3, 3, 2, 0, 0, 1, 0)
        self.assertEqual(test_challenge.run(), 4)

        test_challenge = ChessChallenge(4, 4, 0, 0, 0, 2, 4)
        self.assertEqual(test_challenge.run(), 8)




if __name__ == '__main__':
    unittest.main()
