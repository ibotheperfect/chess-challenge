"""
    This file contains ChessChallenge class and its functions
"""

from threading import Thread, Lock, activeCount


class ChessChallenge:
    def __init__(self, size_m, size_n, king, queen, bishop, rook, knight):
        """

        :param size_m:
        :param size_n:
        :param king:
        :param queen:
        :param bishop:
        :param rook:
        :param knight:
        """
        self.size_m = size_m
        self.size_n = size_n
        # count of empty places
        remaining = size_m * size_n - (king + queen + bishop + rook + knight)
        self.piece_list = list((king * 'K') + (queen * 'Q') +
                               (bishop * 'B') + (rook * 'R') +
                               (knight * 'N') + (remaining * '0'))

        self.mutex = Lock()

        self.success_count = 0

    def unique_permutations(self):
        """
        Since itertools.permutations does not create unique permutations,
        I used this generator to take unique permutation.
        """
        if len(self.piece_list) == 1:
            yield self.piece_list
        set_t = set(self.piece_list)
        for set_el in set_t:
            self.piece_list.remove(set_el)
            for perm in self.unique_permutations():
                yield [set_el] + perm
            self.piece_list.append(set_el)

    def convert_2d(self, input_l):
        """
        Convert 1 dimensional list to 2 dimensional
        :param input_l: list of pieces
        :return: 2 dimensional list
        """
        index_l = 0
        ret = [0] * self.size_n
        for i in range(self.size_n):
            ret[i] = [0] * self.size_m
            for j in range(self.size_m):
                ret[i][j] = input_l[index_l]
                index_l += 1

        return ret

    def is_coord_threaten(self, coord_x, coord_y, board):
        """
        Check if given coordinate is empty
        :param coord_x: horizontal coordinate
        :param coord_y: vertical coordinate
        :param board: 2 dimensional list of board
        :return: true if there is a piece on given coordinates else false
        """
        if coord_y < 0 or coord_x < 0 or \
                coord_x >= self.size_m or coord_y >= self.size_n:
            # this mean coord is out of bounds
            return False
        elif board[coord_y][coord_x] == '0':
            return False
        else:
            return True

    def is_king_threaten(self, coord_x, coord_y, board):
        """
        Check if king threat other pieces
        :param coord_x: horizontal coordinate
        :param coord_y: vertical coordinate
        :param board: 2 dimensional list of board
        :return: true if piece threat any other else false
        """
        return self.is_coord_threaten(coord_x - 1, coord_y - 1, board) or \
            self.is_coord_threaten(coord_x, coord_y - 1, board) or \
            self.is_coord_threaten(coord_x + 1, coord_y - 1, board) or \
            self.is_coord_threaten(coord_x + 1, coord_y, board) or \
            self.is_coord_threaten(coord_x + 1, coord_y + 1, board) or \
            self.is_coord_threaten(coord_x, coord_y + 1, board) or \
            self.is_coord_threaten(coord_x - 1, coord_y + 1, board) or \
            self.is_coord_threaten(coord_x - 1, coord_y, board)

    def is_queen_threaten(self, coord_x, coord_y, board):

        """
        Check if queen threat other pieces
        :param coord_x: horizontal coordinate
        :param coord_y: vertical coordinate
        :param board: 2 dimensional list of board
        :return: true if piece threat any other else false
        """

        # queen is sum of bishop and rook
        return self.is_rook_threaten(coord_x, coord_y, board) or \
            self.is_bishop_threaten(coord_x, coord_y, board)

    def is_bishop_threaten(self, coord_x, coord_y, board):
        """
        Check if bishop threat other pieces
        :param coord_x: horizontal coordinate
        :param coord_y: vertical coordinate
        :param board: 2 dimensional list of board
        :return: true if piece threat any other else false
        """

        # south east
        tmp_x = coord_x + 1
        tmp_y = coord_y + 1
        while tmp_x < self.size_m and tmp_y < self.size_n:
            if self.is_coord_threaten(tmp_x, tmp_y, board):
                return True
            else:
                tmp_x += 1
                tmp_y += 1

        # south west
        tmp_x = coord_x - 1
        tmp_y = coord_y + 1
        while tmp_x > -1 and tmp_y < self.size_n:
            if self.is_coord_threaten(tmp_x, tmp_y, board):
                return True
            else:
                tmp_x -= 1
                tmp_y += 1

        # north west
        tmp_x = coord_x - 1
        tmp_y = coord_y - 1
        while tmp_x > -1 and tmp_y > -1:
            if self.is_coord_threaten(tmp_x, tmp_y, board):
                return True
            else:
                tmp_x -= 1
                tmp_y -= 1

        # north west
        tmp_x = coord_x + 1
        tmp_y = coord_y - 1
        while tmp_x > self.size_m and tmp_y > -1:
            if self.is_coord_threaten(tmp_x, tmp_y, board):
                return True
            else:
                tmp_x += 1
                tmp_y -= 1

        return False

    def is_rook_threaten(self, coord_x, coord_y, board):
        """
        Check if rook threat other pieces
        :param coord_x: horizontal coordinate
        :param coord_y: vertical coordinate
        :param board: 2 dimensional list of board
        :return: true if piece threat any other else false
        """

        # go horizontal

        for i in range(self.size_m):
            if i == coord_x:
                continue
            else:
                if board[coord_y][i] != '0':
                    return True

        # go vertical
        for i in range(self.size_n):
            if i == coord_y:
                continue
            else:
                if board[i][coord_x] != '0':
                    return True
        return False

    def is_knight_threaten(self, coord_x, coord_y, board):
        """
        Check if knight threat other pieces
        :param coord_x: horizontal coordinate
        :param coord_y: vertical coordinate
        :param board: 2 dimensional list of board
        :return: true if piece threat any other else false
        """

        return self.is_coord_threaten(coord_x - 1, coord_y - 2, board) or \
            self.is_coord_threaten(coord_x + 1, coord_y - 2, board) or \
            self.is_coord_threaten(coord_x - 2, coord_y - 1, board) or \
            self.is_coord_threaten(coord_x + 2, coord_y - 1, board) or \
            self.is_coord_threaten(coord_x - 2, coord_y + 1, board) or \
            self.is_coord_threaten(coord_x + 2, coord_y + 1, board) or \
            self.is_coord_threaten(coord_x - 1, coord_y + 2, board) or \
            self.is_coord_threaten(coord_x + 1, coord_y + 2, board)

    def check_board(self, board):
        """
        Check if board is successful sequence
        :param board: 2 dimensional list of board
        :return: return true if board is successful else false
        """

        for i in range(self.size_n):
            for j in range(self.size_m):
                if board[i][j] == '0':
                    continue
                elif board[i][j] == 'K' and self.is_king_threaten(j, i, board):
                    return
                elif board[i][j] == 'Q' and self.is_queen_threaten(j, i, board):
                    return
                elif board[i][j] == 'B' and self.is_bishop_threaten(j, i, board):
                    return
                elif board[i][j] == 'R' and self.is_rook_threaten(j, i, board):
                    return
                elif board[i][j] == 'N' and self.is_knight_threaten(j, i, board):
                    return

        self.mutex.acquire()
        self.success_count += 1
        self.mutex.release()

    def run(self):
        """
        This functions do everything
        :return: number of successful sequences
        """
        self.success_count = 0  # in case of rerun

        for i in self.unique_permutations():
            board = self.convert_2d(i)
            # It would be better to control maximum number of threads
            Thread(target=self.check_board, args=(board,)).start()

        while activeCount() != 1:
            pass

        return self.success_count

